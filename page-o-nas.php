<?php 
/*
Template Name: O nas page
*/

get_header();?>

<section class="about">
    <div class="about-hero">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="about-box">
                        <h1 class="about-box__title"><?php the_field('hlavny_nadpis');?></h1>
                        <p class="about-box__subtitle"><?php the_field('hlavny_podnadpis');?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="offers">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="offers__title-wraú">
                        <h3 class="offers__title"><?php the_field('hlavny_nadpis_ponukame');?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="results-bcg offers-bcg">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="results__title-wrap">
                                    <div class="results__img-wrap">
                                        <img class="results__img" src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/hviezda_mala_biela_ikonka.svg'?>" alt="">
                                    </div> 
                                    <h1 class="results__title"><?php the_field('ponukame_nadpis'); ?></h1>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="references-detail-items__group references-detail-items__group-about">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="references-detail-items__text-wrap">
                                    <p class="references-detail-items__text"><?php the_field('ponukame_text'); ?></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="references-detail-items__img-wrap">
                                    <img class="references-detail-items__img" src="<?php  echo get_field('ponukame_obrazok') ['url']; ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    </div>
    <div class="properties">
        <div class="container">
            <div class="row">
                     <?php
                        if(have_rows('vlastnosti') ):
                            while( have_rows('vlastnosti') ) : the_row();
                    ?>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="properties-box">
                                <div class="properties-box__img-wrap">
                                    <img class="properties-box__img" src="<?php echo get_sub_field('obrazok_vlastnosti') ['url']; ?>" alt="">
                                </div>
                                <div class="properties-box__title-wrap">
                                    <h1 class="properties-box__title">
                                        <?php the_sub_field('nadpis_vlastnosti'); ?>
                                    </h1>
                                </div>
                                <div class="properties-box__list-wrap">
                                    <ul class="properties-box__list">
                                            <?php
                                                if(have_rows('vlastnosti_itemy') ):
                                                    while( have_rows('vlastnosti_itemy') ) : the_row();
                                                ?>
                                                <li class="properties-box__list-item"><?php the_sub_field('pridaj_item_vlastnosti');?></li>
                                            <?php
                                                endwhile;
                                                endif;
                                            ?>                 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                        endwhile;
                        endif;
                    ?>
            </div>
        </div>
    </div>
    <div class="quality">
         <div class="results-bcg quality-bcg">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="results__title-wrap">
                            <div class="results__img-wrap">
                                <img class="results__img" src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/hviezda_mala_biela_ikonka.svg'?>" alt="">
                            </div>   
                            <h1 class="results__title"><?php the_field('nadpis_sekcie_akordeon');?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                 <div class="col-sm-12 col-md-12 col-lg-9">
                     <div class="acordeon-wrapper">
                        <?php
                            if(have_rows('akordeon') ):
                                while( have_rows('akordeon') ) : the_row();
                            ?>
                            <div class="acordeon-wrap">
                                <dl class="acordeon">
                                    <dt class="acordeon-title">               
                                        <a href=""><?php the_sub_field('akordeon_nadpis'); ?></a>
                                    </dt>
                                    <dd class="acordeon-content">
                                        <ul class="properties-box__list">
                                            <?php
                                                if(have_rows('akordeon_items') ):
                                                    while( have_rows('akordeon_items') ) : the_row();
                                                ?>
                                                <li class="properties-box__list-item"><?php the_sub_field('pridaj_idem');?></li>
                                            <?php
                                                endwhile;
                                                endif;
                                            ?>

                                        </ul>
                                        <?php the_sub_field('popis_aktivity'); ?>
                                    </dd>
                                <dl>
                            </div>
                        <?php
                            endwhile;
                            endif;
                        ?>
                     </div>    
                </div>
            </div>

        </div>
        

    

    </div>
    <div class="our-team">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="out-team__title-wrap">
                        <h1 class="our-team__title"><?php the_field('nas_tym_nadpis');?></h1>
                    </div>
                </div>
                <?php
                    if(have_rows('zamestnanec') ):
                        while( have_rows('zamestnanec') ) : the_row();
                    ?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="our-team-item">
                            <div class="our-team-item__img-wrap">
                                <img class="our-team-item__img" src="<?php echo get_sub_field('fotka_zamestnanca') ['url']; ?>" alt="">
                            </div>
                            <div class="our-team-item__name-wrap">
                                <h1 class="our-team-item__name"><?php the_sub_field('meno_zamestnanca'); ?></h1>
                            </div>
                            <div class="our-team-item__position-wrap">
                                <p class="our-team-item__position"><?php the_sub_field('pozicia_zamestnanca'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                    endif;
                ?>

               
            </div>
        </div>

    </div>
    <div class="partners">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="partners__title-wrap">
                        <h1 class="partners__title"> <?php the_field('partneri_nadpis'); ?> </h1>
                    </div>
                </div>
                 <?php
                    if(have_rows('partneri') ):
                        while( have_rows('partneri') ) : the_row();
                    ?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="partners-box">
                            <div class="partners-box__img-wrap">
                                <a class="partners-box__link" href="<?php the_sub_field('link_na_stranku');?>" target="_blank"> <img class="partners-box__img" src="<?php echo get_sub_field('obrazok_partnera') ['url']; ?>" alt=""></a>        
                            </div>
                            <div class="partners-box__title-wrap">
                                <a class="partners-box__link" href="<?php the_sub_field('link_na_stranku');?>" target="_blank"><p class="partners-box__title"><?php the_sub_field('nazov_partnera'); ?></p></a>       
                            </div>
                            <div class="partners-box__subtitle-wrap">
                                <p class="partners-box__subtitle"><?php the_sub_field('popis_partnera'); ?></p>
                            </div>
                        </div>
                    </div>
                     
                <?php
                    endwhile;
                    endif;
                ?>
               
            </div>
        </div>

    </div>
    <div class="about-company">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-company__title-wrap">
                        <h1 class="about-company__title"> <?php the_field('priestory_kde_pracujeme_nadpis');?></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if(have_rows('priestory') ):
                    while( have_rows('priestory') ) : the_row();
                ?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="about-company__img-wrap">
                            <img class="about-company__img" src="<?php echo get_sub_field('obrazok_priestoru') ['url']; ?>">
                        </div>    
                    </div>
                <?php
                    endwhile;
                    endif;
                ?>

            </div>
        </div>
    </div>
</section>

<?php get_footer();?>