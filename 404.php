<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package default-theme
 */

get_header();
?>

	<section class="page-none">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="page-none-box">
						<h1><?php echo __('Nič sa nenašlo :(', 'default-theme');?></h1> 

					</div>
				</div>
			</div>
		</div>

	</section>

<?php
get_footer();
