��    &      L      |      |     }     �     �  @   �     �     
       &     a   F     �     �     �     �     �     �  
          +   5  	   a     k     {  
   �     �  "   �     �     �  
   �               .     ;     J  	   \     f     {     �     �  �  �     u     }  
   �  D   �     �     �     �  .     j   ;  
   �     �     �     �     �     �     �     	  (   $	     M	     V	     e	     m	  
   u	     �	     �	     �	  
   �	  	   �	     �	     �	     �	     �	     
     
     
  	   2
  	   <
   Adresa spoločnosti Bankové spojenie Chcem sa poradiť Eurofondy na podnikanie, aktuálne výzvy a čerpanie eurofondov Fakturačné údaje Kontakt Mám záujem Máte nápad a chcete ho realizovať ? Máte nápad/ideu a chcete ich 
                                realizovať s pomocou eurofondov? Napíšte nám Naše aktivity Naše služby Naši partneri Nič sa nenašlo :( Náš komentár k projektu Náš tým O čo v projekte išlo? Politiku kvality a Enviromentálnu politiku Ponúkame Povedali o nás Poďme na to Prečo my? Priestory kde pracujeme Prihláste sa na odber newslettera Realizované projekty Realizácie projektov Referencie S kým spolupracujeme Výsledky projektu Zistiť viac Zobraz všetky Zobraziť všetky rozpočet spokojných klientov zrealizovaných projektov Čítaj ďalej Čítať ďalej Project-Id-Version: _s 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/_s
Last-Translator: 
Language-Team: English (UK)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2021-06-07 11:08+0000
PO-Revision-Date: 2021-06-17 08:11+0000
X-Generator: Loco https://localise.biz/
X-Domain: _s
Language: en_GB
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.5.2; wp-5.7.2 Address Bank connection
 Contact us Eurofunds for business, current challenges and drawing on Eurofunds
 Billing information
 Contact I am interested
 Do you have an idea and want to implement it?
 Have an idea and you want it
                                to be implemented with the help of Eurofunds? Contact us Our activities Our services Our partners Sorry, nothing found :( Our comment Our team What was the project about?
 Quality policy and Environmental policy
 We offer What they said Lets go Why us? Our places Subscribe to the newsletter
 Realized projects
 Realization of projects
 References Work with Project results More Show all Show all budget satisfied clients
 completed projects Read more Read more 