var gulp = require("gulp"),
  gutil = require("gulp-util");
sass = require("gulp-sass");
uglyfly = require("gulp-uglyfly");
cssnano = require("gulp-cssnano");
sourcemaps = require("gulp-sourcemaps");
rename = require("gulp-rename");
watch = require("gulp-watch");
rimraf = require("gulp-rimraf");
newer = require("gulp-newer");
notify = require("gulp-notify");
browserSync = require("browser-sync").create();
imagemin = require("gulp-imagemin");
//autoprefixer = require('gulp-autoprefixer')
//livereload = require('gulp-livereload')
concat = require("gulp-concat");
//electron = require('electron-connect').server.create();
// Name of working theme folder
(scss = "src/scss/"),
  (js = "src/js/"),
  (img = "src/img/"),
  (php = "*.php"),
  (languages = "src/languages/");
reload = browserSync.reload;

var sassSources = ["src/scss/style.scss"];

gulp.task("sass", function () {
  return gulp
    .src("src/scss/style.scss")
    .pipe(sass({ includePaths: ["node_modules"] }))
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("dist/css"))
    .pipe(reload({ stream: true }));
});

gulp.task("compress", function () {
  return gulp
    .src("src/js/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(uglyfly())
    .pipe(gulp.dest("dist/js"))
    .pipe(reload({ stream: true }));
});

gulp.task("cssminify", function () {
  return gulp
    .src("dist/css/style.css")
    .pipe(sourcemaps.init())
    .pipe(cssnano())
    .pipe(sourcemaps.write("."))
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest("dist/css"))
    .pipe(reload({ stream: true }));
});

gulp.task("imgsquash", function () {
  return gulp
    .src("src/img/*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/img/"))
    .pipe(reload({ stream: true }));
});

gulp.task("watch", function () {
  browserSync.init({
    browser: "google chrome",
    proxy: "http://localhost:8888/razdva-tema/",
  });
  gulp.watch("./src/scss/**", gulp.series("sass"));
  gulp.watch("./dist/css/style.css", gulp.series("cssminify"));
  gulp.watch("./src/js/*.js", gulp.series("compress"));
  gulp.watch("./src/img/*", gulp.series("imgsquash"));
  gulp.watch("*.php").on("change", reload);
  gulp.watch("**/*.php").on("change", reload);
  gulp.watch("./dist/js/scripts.min.js").on("change", reload);
  gulp.watch("./dist/css/style.css").on("change", reload);
});
