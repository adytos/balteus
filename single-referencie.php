<?php 
/*
Template Name: Referencia single page
*/
get_header();?>


<section class="references-detail">
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb__wrap">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() . '/src/img/other/home_ikonka.svg' ?>" alt=""></a></li>
                            <li class="breadcrumb-item"><a class="breadcrumb-link" href="<?php echo get_permalink( icl_object_id(115, 'page', false) );?>"><?php echo __('Referencie', 'default-theme');?></a></li>
                            <li class="breadcrumb-item"><a class="breadcrumb-link breadcrumb-link--active" href=""><?php the_title(); ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="referecnes-detail__img-wrap">
                    <?php echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'references-detail__img' )); ?>

                    
                </div>
                <div class="references-detail__title-wrap">
                    <h1 class="references-detail__title"><?php the_title();?></h1>         
                </div>  
                <div class="references-detail__company-wrap">
                    <h3 class="references-detail__company"><?php the_field('nazov_firmy');?></h3>            
                    
                </div>   
            </div>
        </div>
    </div>
     <div class="results-bcg references-detail-bcg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="results__title-wrap">
                        <div class="results__img-wrap">
                            <img class="results__img" src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/hviezda_mala_biela_ikonka.svg'?>" alt="">
                        </div>   
                        <h1 class="results__title"> <?php the_field('o_co_islo_v_projekte_nadpis');?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="references-detail__about-wrap">
                    <p class="references-detail__about"><?php the_field('o_co_islo_v_projekte');?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="references-detail-items">
                <?php
                if(have_rows('referencia_item') ):
                    while( have_rows('referencia_item') ) : the_row();
                ?>
                <div class="results-bcg references-detail-items-bcg">
                     
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="results__title-wrap">
                                    <div class="results__img-wrap">
                                        <img class="results__img" src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/hviezda_mala_biela_ikonka.svg'?>" alt="">
                                    </div> 
                                    <h1 class="results__title"><?php the_sub_field('referencia_item_nadpis'); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="references-detail-items__group">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="references-detail-items__text-wrap">
                                    <p class="references-detail-items__text"><?php the_sub_field('referencia_item_text'); ?></p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="references-detail-items__img-wrap">
                                    <img class="references-detail-items__img" src="<?php echo get_sub_field('referencia_item_obrazok') ['url']; ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            <?php
                  endwhile;
                endif;
            ?>

    
</section>



<?php  

    get_template_part( 'sections/section', 'results' );

    get_template_part( 'sections/section', 'comments' );

    get_template_part( 'sections/section', 'recommend' );

?>


<?php get_footer();?>