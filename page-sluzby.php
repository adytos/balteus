<?php
/*
Template Name: Services page
*/
get_header();?>

<section class="services-hero">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-11 offset-lg-1">
                    <div class="services-hero-box">
                        <h1 class="services-hero-box__title"> <?php the_title();?></h1>
                        <div class="services-hero-box__content">
                            <?php the_content();?>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
</section>
<section class="services-items">
     <?php
             if(have_rows('sluzby_item'  ) ):
                    while( have_rows('sluzby_item') ) : the_row();
                
                ?>

                <div class="test">
                    <div class="services-items-bcg results-bcg references-detail-items-bcg ">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="results__title-wrap">
                                    <div class="results__img-wrap">
                                        <img class="results__img" src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/hviezda_mala_biela_ikonka.svg'?>" alt="">
                                    </div> 
                                    <h1 class="results__title"><?php the_sub_field('sluzby_item_nadpis'); ?></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="references-detail-items__group">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="references-detail-items__text-wrap">
                                    <p class="references-detail-items__text">
                                        <?php the_sub_field('sluzby_item_text'); ?>
                                    </p>
                                    
                                </div>
                                
                                
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="references-detail-items__img-wrap">
                                    <img class="references-detail-items__img" src="<?php echo get_sub_field('sluzby_item_obrazok') ['url']; ?>" alt="">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-9">
                                    <?php
                                    if(have_rows('sluzby_akorderon') ):
                                        while( have_rows('sluzby_akorderon') ) : the_row();
                                    ?>
                                    <div class="acordeon-wrap">
                                        <dl class="acordeon">
                                            <dt class="acordeon-title"><a href=""><?php the_sub_field('nadpis_sluzby_akordeon'); ?></a></dt>
                                            <dd class="acordeon-content">
                                                <div><?php the_sub_field('text_sluzby_akordeon'); ?></div>
                                            </dd>
                                        <dl>
                                    </div>
                                <?php
                                    endwhile;
                                    endif;
                                ?>

                                </div>
                            <div class="col-12">
                                <div class="references-detail__btn btn__main-wrap">
                                    <a class="btn__main" href="<?php the_sub_field('link');?>"> <?php echo __('Mám záujem', 'default-theme');?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
                
                
            <?php
                  endwhile;
                endif;
            ?>

</section>
<section class="authorization">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="authorization__title"><?php the_field('opravneni_ziadatelia_nadpis');?></h1>
                <p class="authorization__content"><?php the_field('popis_ziadatelov');?></p>
            </div>
        </div>
    </div>
    <div class="authorization-box__bcg">
        <div class="authorization-box__wrap">
            <div class="container">
                <div class="row">
                    <?php
                        if(have_rows('opravnenia') ):
                            while( have_rows('opravnenia') ) : the_row();
                        ?>
                    <div class="col-sm-12 col-md-6 col-lg">
                            <div class="authorization-box">
                                <div class="authorization-box__img-wrap">
                                    <img class="authorization-box__img" src="<?php echo get_sub_field('obrazok_opravnenia') ['url']; ?>">
                                </div>
                                <div class="authorization-box__content-wrap">
                                    <p class="authorization-box__content"><?php the_sub_field('popis_opravnenia'); ?></p>
                                </div>              
                            </div>    
                    </div>
                    <?php
                        endwhile;
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_template_part( 'sections/section', 'contact' );?>

<?php get_footer();?>