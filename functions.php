<?php

/**
 * default-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package default-theme
 */

if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

if (!function_exists('default_theme_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function default_theme_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on default-theme, use a find and replace
		 * to change 'default-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('default-theme', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__('Primary', 'default-theme'),
				'menu-2' => __('Footer menu'),
				'menu-3' => __('Langs'),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'default_theme_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'default_theme_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function default_theme_content_width()
{
	$GLOBALS['content_width'] = apply_filters('default_theme_content_width', 640);
}
add_action('after_setup_theme', 'default_theme_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function default_theme_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'default-theme'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'default-theme'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'default_theme_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function default_theme_scripts()
{
		//Jquery
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('default-theme-slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array("jquery"), _S_VERSION, true);


	//styles
	wp_enqueue_style('default-theme-slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');

	wp_enqueue_style('default-theme-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('default-theme-style', 'rtl', 'replace');
	//JS
	wp_enqueue_script('default-theme-animations', get_template_directory_uri() . '/js/animation.js', array("jquery"), _S_VERSION, true);
	//wp_enqueue_script('default-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

	wp_enqueue_style('default-theme-css', get_template_directory_uri() . '/dist/css/style.css', array(), time(), "all");

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'default_theme_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}


if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'Nastavenie témy',
		'menu_title'	=> 'Nastavenie témy',
		'menu_slug' 	=> 'theme-settings'
	));
}

//Create new post type in admin menu

add_action('init', 'create_post_type');

function create_post_type()
{
	register_post_type(
		'referencie',
		array(
			
			'labels' => array(
				'name' => __('Referencie'),
				'singular_name' => __('Referencie'),
				'add_new' => __('Pridať nový '),
				'add_new_item' => __('Pridať nový ')
			),
			'supports' => array( 'thumbnail','title' ),
			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-hammer',
			//'supports' => false,
			'menu_position' => 5,



		)
	);
	
}	

//Excerpt length
function wp_example_excerpt_length($length)
{
	return 15;
}
add_filter('excerpt_length', 'wp_example_excerpt_length');

function new_excerpt_more($more)
{
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');



//Only show products in the front-end search results
function lw_search_filter_pages($query)
{
	if ($query->is_search) {
		$query->set('post_type', array('post', 'referencie') );
		$query->set('wc_query', 'product_query');
	}
	return $query;
}

add_filter('pre_get_posts', 'lw_search_filter_pages');





