<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package default-theme
 */
/*
Template Name: Referencie page
*/
get_header();
?>

	
<section class="references">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="references__title-wrap">
                    <h1 class="references__title">
                        <?php  if ( pll_current_language() == 'sk'  ){
                                    the_field('nadpis_referencie', 115); 
                             } else{
                                    the_field('nadpis_referencie', 379);
                             } 
                        ?>
                    </h1>
                </div>
                <div class="references__content-wrap">
                    <div class="references__content">
                        <?php  if ( pll_current_language() == 'sk'  ){
                                    the_field('popis_referencie', 115); 
                             } else{
                                    the_field('popis_referencie', 379);
                             } 
                        ?>
						
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="references-items pc-view">
    <div class="container">   
        <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="referencie" scroll_distance="-50" max_pages="1" posts_per_page="6" transition_container_classes="row"  button_label="Zobraz ďalšie"]');?>                                 
    </div>
</section>

<section class="references-items mobile-view">
    <div class="container">   
        <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="referencie" scroll_distance="-50" max_pages="1" posts_per_page="2" transition_container_classes="row"  button_label="Zobraz ďalšie"]');?>                                 
    </div>
</section>



<?php
get_footer();
