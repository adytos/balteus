<section class="companies">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="companies__title-wrap">
                    <h1 class="companies__title"><?php the_field('spolocnosti_nadpis');?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="company-box-js">
                    <?php
                if(have_rows('spolocnosti') ):
                    while( have_rows('spolocnosti') ) : the_row();
                ?>
                <div class="col-sm-12 col-md-12 col-lg">
                        <div class="company-box ">
                            <div class="company-box-wrap">
                                
                                <div class="company-box__title-wrap">
                                    <a href="<?php the_sub_field('link_na_spolocnost');?>" target="_blank" class="company-box__title"><?php the_sub_field('nazov_spolocnosti'); ?></a>
                                </div>
                                <p class="company-box__content"><?php the_sub_field('popis_spolocnosti'); ?></p>
                            </div>    
                        </div>    
                </div>
                <?php
                    endwhile;
                    endif;
                ?>

                </div>
            </div>
        </div>
    </div>
</section>