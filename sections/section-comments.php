<section class="comments">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="comments__title-wrap">
                    <h1 class="comments__title"><?php echo __('Náš komentár k projektu','default-theme');?></h1>
                </div>
                <div class="comments__item-wrap">
                    <div class="comments__img-wrap">
                        <img class="comments__img"  src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/mala_hviezda_ikonka.svg' ?>" alt="">
                    </div>    
                    <p class="comments__item">
                        <?php the_field('komentar_k_projektu');?></p>
                </div>
            </div>
        </div>
    </div>
</section>