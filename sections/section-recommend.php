<section class="recommend">
    <div class="recomment-wrapper">
        <div class="recommend-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-8">
                        <div class="recommend__title-wrap">
                            <h1 class="recommend__title">
                                
                                <?php echo __('Máte nápad a chcete ho realizovať ?', 'default-theme');?>
                            </h1>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="recommend__btn-wrap">
                            <a class="recommend__btn btn__main" href="/kontakt/#contact-form"> <?php echo __('Chcem sa poradiť', 'default-theme');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>