<section class="results">
    <div class="results-bcg references-detail-bcg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="results__title-wrap">
                        <div class="results__img-wrap">
                            <img class="results__img" src="<?php echo get_template_directory_uri() . '/src/img/detail_referencie/hviezda_mala_biela_ikonka.svg'?>" alt="">
                        </div> 
                        <h1 class="results__title"><?php the_field('vysledky_projektu_nadpis');?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php
                if(have_rows('vysledky_projektu') ):
                    while( have_rows('vysledky_projektu') ) : the_row();
                ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="results-item">
                        <div class="results-item__img-wrap">
                            <img class="results-item__img" src="<?php echo get_sub_field('vysledok_img') ['url']; ?>" alt="">
                        </div>
                        <div class="results-item__text-wrap">
                            <h5 class="results-item__text"><?php the_sub_field('vysledok_item'); ?></h5>
                        </div>
                    </div>
                </div>
            <?php
                  endwhile;
                endif;
            ?>

        </div>
    </div>
</section>