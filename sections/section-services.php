<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="services__title-wrap">
                    <h3 class="services__title"><?php the_field('nase_sluzby_nadpis');?></h3>
                </div>
            </div>
        </div>        
        <div class="row">
            
                 <?php
                    if(have_rows('sluzby_item_front') ):
                        while( have_rows('sluzby_item_front') ) : the_row();
                    ?>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="services-box">
                            <div class="services-box__img-wrap">
                                <img class="services-box__img" src="<?php echo get_sub_field('sluzby_obrazok_front') ['url']; ?>" alt="">
                            </div>
                            <div class="services-box__title-wrap">
                                <h5 class="services-box__title"><?php the_sub_field('sluzby_item_nadpis_front'); ?></h5>
                            </div>
                            <div class="services-box__content-wrap">
                                <p class="services-box__content">
                                    <?php the_sub_field('sluzby_item_text_front');?>
                                </p>
                                 
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                    endif;
                ?>
								
                   
            
        </div> 
        <div class="row">
            <div class="col">
                <div class="btn__main-wrap">
                    <a class="btn__main" href="<?php echo get_permalink( icl_object_id(101, 'page', false) );?>"><?php echo __('Zistiť viac', 'default-theme');?></a>
                </div>
            </div>
        </div>  
    </div>
</section>