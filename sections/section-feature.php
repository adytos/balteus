<section class="feature">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title__second-wrap">
                    <h5 class="title__second"><?php the_field('preco_my_nadpis');?></h5>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
                if(have_rows('vlastnosti') ):
                    while( have_rows('vlastnosti') ) : the_row();
                ?>
             <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                <div class="feature-item">
                    <div class="feature-item__img-wrap">
                        <img class="feature-item__img" src="<?php echo get_sub_field('obrazok_vlastnosti')['url']; ?>" alt="">
                    </div>
                    <div class="feature-item__title-wrap">
                        <h5 class="feature-item__title"><?php the_sub_field('nadpis_vlastnosti');?></h5>
                    </div>
                </div>
            </div>
            <?php
                  endwhile;
                endif;
            ?>

            
        </div>       
    </div>
    <div class="feature-number feature-numbers">
        <div class="container">
            <div class=row>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="feature-number-item" align="center">
                        <div class="feature-number-item__title-wrap">
                            <h5 class="feature-number-item__title"> <span id="countup" class="counter myTargetElement demo" data-num="<?php the_field('rozpocet_projektov');?>">0</span>€</h5> 
                        </div>
                        <div class="feature-number-item__subtitle-wrap">
                            <h5 class="feature-number-item__subtitle"><?php the_field('rozpocet_projektov_nadpis');?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="feature-number-item" align="center">
                        <div class="feature-number-item__title-wrap">
                            <h5 class="feature-number-item__title counter myTargetElement demo" id="countup2" data-num="<?php the_field('realizovane_projekty');?>">0</h5>
                        </div>
                        <div class="feature-number-item__subtitle-wrap">
                            <h5 class="feature-number-item__subtitle"><?php the_field('realizovane_projekty_nadpis');?></h5>
                        </div>
                    </div>
                </div>
                 <div class="col-sm-12 col-md-6 col-lg-3" >
                    <div class="feature-number-item" align="center">
                        <div class="feature-number-item__title-wrap">
                            <h5 class="feature-number-item__title counter myTargetElement demo" id="countup3" data-num="<?php the_field('rokov_na_trhu');?>">0</h5>
                        </div>
                        <div class="feature-number-item__subtitle-wrap">
                            <h5 class="feature-number-item__subtitle"><?php the_field('rokov_na_trhu_nadpis');?></h5>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
</section>



