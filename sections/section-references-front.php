<section class="front-references">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="front-references__title-wrap">
                    <h5 class="front-references__title"><?php the_field('realizacie_projektov_nadpis');?></h5>
                </div>           
            </div>
        </div>
    </div>  
    <div class="front-references-pc pc-view">
        <div class="container">
             <div class="row">
                <?php

                    $args = [
                        'post_type' => 'referencie',
                        "posts_per_page" => 3,
                        

                    ];
                    $query = new WP_Query($args);

                    while ($query->have_posts()) : $query->the_post();
                    ?>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="references-box">
                                <div class="references-box__img-wrap">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'references-box__img' )); ?>
                                    </a>
                                </div>
                                <div class="references-box__content">
                                    <h1 class="references-box__title">
                                           <a href="<?php the_permalink();?>">  <?php the_title() ?></a> </h1>                                   
                                    <div class="references-box__detail-wrap">
                                        <a class="references-box__detail" href="<?php the_permalink(); ?>"> <?php echo __('Čítaj ďalej', 'default-theme');?><img class="references-box__icon" src="<?php echo get_template_directory_uri() . '/src/img/other/arrow-right.svg' ?>" alt=""></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php wp_reset_postdata() ?>
                    <?php

                    endwhile;

                    ?>

             </div>
        </div>
    </div> 
    <div class="front-references-mobile mobile-view">
        <div class="container">
             <div class="row">
                <?php

                    $args = [
                        'post_type' => 'referencie',
                        "posts_per_page" => 2,
                       

                    ];
                    $query = new WP_Query($args);

                    while ($query->have_posts()) : $query->the_post();
                    ?>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="references-box">
                                <div class="references-box__img-wrap">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php echo get_the_post_thumbnail($post->ID, '', array( 'class' => 'references-box__img' )); ?>
                                    </a>
                                </div>
                                <div class="references-box__content">
                                    <h1 class="references-box__title">
                                           <a href="<?php the_permalink();?>">  <?php the_title() ?></a> </h1>                                   
                                    <div class="references-box__detail-wrap">
                                        <a class="references-box__detail" href="<?php the_permalink(); ?>"> <?php echo __('Čítaj ďalej', 'default-theme');?><img class="references-box__icon" src="<?php echo get_template_directory_uri() . '/src/img/other/arrow-right.svg' ?>" alt=""></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php wp_reset_postdata() ?>
                    <?php

                    endwhile;

                    ?>

             </div>
        </div>
    </div>
    <div class="container">   
        <div class="row">
            <div class="col-12">
                <div class="front-references__btn-wrap">
                    <a class="front-references__btn btn__main" href="<?php echo get_permalink( icl_object_id(115, 'page', false) );?>"><?php echo __('Zobraz všetky', 'default-theme');?></a>
                </div>
            </div>
        </div>
    </div>
</div>