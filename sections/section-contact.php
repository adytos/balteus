    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="contact__img-wrap">
                    <img class="contact__img" src="<?php echo get_template_directory_uri() . '/src/img/other/contact-img-obalka.svg'?>" alt="">
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="contact-form" id="contact-form">
                    <h2 class="contact-form-title"><?php echo __('Napíšte nám','default-theme');?></h2>
                    <?php	if ( pll_current_language() == 'sk'  ) {
								echo do_shortcode('[contact-form-7 id="39" title="Kontaktny formular"]');
							} else {  echo do_shortcode('[contact-form-7 id="433" title="Kontaktny formular_en"]');
							 }
						?>
                    
                </div>
            </div>
        </div>
    </div>
    