<section class="about-front">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="about-front-box">
                    <div class="about-front-box__title-wrap">
                        <h2 class="about-front-box__title"> <?php the_field('o_nas_nadpis');?></h2>
                    </div>
                    <div class="about-front-box__content-wrap">
                        <p class="about-front-box__content">
                            <?php the_field('o_nas_podnadpis');?>
                        </p>
                    </div>
                    <div class="about-front-box__btn-wrap btn__main-wrap">
                         <a href="<?php echo get_permalink( icl_object_id(42, 'page', false) ); ?>" class="about-front-box__btn btn__main"><?php _e('Zistiť viac', 'default-theme');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>