<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="hero-box">
                    <div class="hero-box__title-wrap">
                        <h1 class="hero-box__title"><?php the_field('hlavny_nadpis');?></h1>
                    </div>
                    <div class="hero-box__content-wrap">
                        <p class="hero-box__content"><?php the_field('hlavny_podnadpis');?></p>
                    </div>
                    <div class="hero-box__btn-wrap btn__main-wrap">
                        
                        <a class="hero-box__btn btn__main" href="<?php echo get_permalink( icl_object_id(101, 'page', false) );?>"><?php echo __('Poďme na to', 'default-theme');?> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>