<section class="blog-front">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="blog-front__headline"><?php the_field('blog_nadpis');?></h1>
            </div>
        </div>  
    </div>  
    <div class="blog-front-pc pc-view">
        <div class="container">
             <div class="row front-blog-items">   
            <?php

            $args = [
                'post_type' => 'post',
                "posts_per_page" => 3,
                //'order' => 'ASC'
            ];
            $query = new WP_Query($args);

            while ($query->have_posts()) : $query->the_post();
            ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="blog-front-boxs">
                        <a class="blog-front-boxs__link" href="<?php the_permalink(); ?>">
                            <div class="blog-front-single">
                                <div class="blog-front__content">
                                    <div class="blog-front__title-wrap">
                                        <?php

                                        the_title('<h1 class="blog-front__title">', '</h1>');

                                        ?>
                                    </div>
                                    <div class="blog-front__text">
                                        <p> <?php if (is_page()) {
                                                the_excerpt();
                                            } ?>
                                        </p>
                                    </div>                        
                                </div>
                                <div class="blog-front__button-wrap">
                                    <a class="blog-front__button blog__button" href="<?php the_permalink(); ?>"><?php echo __('Čítaj ďalej', 'default-theme');?> <img class="references-box__icon" src="<?php echo get_template_directory_uri() . '/src/img/other/arrow-right.svg' ?>" > </a>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <?php wp_reset_postdata() ?>
            <?php

            endwhile;

            ?>
           
        </div>

        </div>

    </div>
      <div class="blog-front-mobile mobile-view">
          <div class="container">
               <div class="row front-blog-items">   
            <?php

            $args = [
                'post_type' => 'post',
                "posts_per_page" => 2,
                //'order' => 'ASC'
            ];
            $query = new WP_Query($args);

            while ($query->have_posts()) : $query->the_post();
            ?>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="blog-front-boxs">
                        <a class="blog-front-boxs__link" href="<?php the_permalink(); ?>">
                            <div class="blog-front-single">
                                <div class="blog-front__content">
                                    <div class="blog-front__title-wrap">
                                        <?php

                                        the_title('<h1 class="blog-front__title">', '</h1>');

                                        ?>
                                    </div>
                                    <div class="blog-front__text">
                                        <p> <?php if (is_page()) {
                                                the_excerpt();
                                            } ?>
                                        </p>
                                    </div>                        
                                </div>
                                <div class="blog-front__button-wrap">
                                    <a class="blog-front__button blog__button" href="<?php the_permalink(); ?>"><?php echo __('Čítaj ďalej', 'default-theme');?> <img class="references-box__icon" src="<?php echo get_template_directory_uri() . '/src/img/other/arrow-right.svg' ?>" > </a>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <?php wp_reset_postdata() ?>
            <?php

            endwhile;

            ?>
           
        </div>

          </div>

      </div> 
     <div class="container">   
        <div class="row">
             <div class="col-12">
                <div class="blog-front__btn-wrap">
                    <a class="blog-front__btn btn__main" href="<?php echo get_permalink( icl_object_id(87, 'page', false) );?>"><?php echo __('Zobraz všetky', 'default-theme');?></a>
                </div>
            </div>

        </div>
    </div>
</section>