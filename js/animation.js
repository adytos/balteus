$(document).ready(function () {
  //BACK TO TOP
  var backToTop = $('<a>', {
    href: '#home',
    class: 'back-to-top',
    html: '<i class="fas fa-arrow-up"></i>'

  });

  backToTop.hide()
    .appendTo('body')
    .on('click', function () {
      $('html').animate({ scrollTop: 0 }, 1000);
    });
  var win = $(window);
  win.on('scroll', function () {
    if (win.scrollTop() >= 800) backToTop.fadeIn();
    else backToTop.fadeOut();
  })


  $(".company-box-js").slick({
    slidesToShow: 1,
    arrows: true,
    infinite: true,
    autoplay: true,
    dots: false,
    fade: true,
    pauseOnFocus: true,
    pauseOnHover: true,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          centerMode: true,
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          centerMode: true,
          slidesToShow: 1,
        },
      },
    ],
  });


  $(".search__link").on("click", function (e) {
    e.preventDefault();
    $(".search-max").toggleClass("show");
  });

  var list = $(".acordeon");
  list.find("dd").hide();
  list.find("dt").on("click", function (e) {
    $(this).toggleClass("acordeon-title--opened");

    $(this).next().slideToggle();
    e.preventDefault();
  });

  $(".menu-toggle").on("click", function (e) {
    e.preventDefault();
    $(".main-navigation").toggleClass("toggled").toggleClass("menu-opened");
    $("body")
      .toggleClass("toggled-view");

  });


  var shrinkHeader = 200;
  $(window).scroll(function () {
    var scroll = getCurrentScroll();

    if (scroll >= shrinkHeader) {
      $(".site-header").addClass("shrink");
      $(".header__logo-main").attr(
        "src",
        "//localhost:3000/razdva-tema/wp-content/themes/default-theme/src/img/other/balteus-shrink.svg"
      );
      $(".pre-header").hide();
    } else {
      $(".site-header").removeClass("shrink");
      $(".header__logo-main").attr(
        "src",
        "//localhost:3000/razdva-tema/wp-content/themes/default-theme/src/img/other/Logo_Balteus.svg"
      );
      $(".pre-header").show();
    }
  });

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }



  counter(); // call counter function

  function counter() {
    $(".counter").each(function () {
      $(this)
        .prop("Counter", 0)
        .animate(
          {
            Counter: parseInt($(this).data('num'))
          },
          {
            duration: 2000,
            easing: "swing",
            step: function (now) {
              now = parseInt(now);
              $(this).text(formatter.format(Math.ceil(now)));
            }
          }
        );
    });
  }
});


// Number formatter
var formatter = new Intl.NumberFormat("sk-SK", {
  maximumFractionDigits: 0,
  minimumFractionDigits: 0
});

