<?php 

/*
Template Name: Kontakt page
*/

get_header();?>

<section class="contact">
    <div class="contact-hero">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact-hero__title-wrap">
                        <h5 class="contact-hero__title"><?php the_field('kontakt_nadpis');?></h5>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 offset-lg-2">
                    <div class="contact-hero__item-wrap">
                        <a class="contact-hero__item" href="tel:<?php the_field('telefon', 'option');?>"><img class="contact-hero__img" src="<?php echo get_template_directory_uri() . '/src/img/other/Mobil_ikonka.svg'?>" alt=""><?php the_field('telefon', 'option');?></a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="contact-hero__item-wrap">
                        <a class="contact-hero__item" href="mailto:<?php the_field('email', 'option');?>"><img class="contact-hero__img" src="<?php echo get_template_directory_uri() . '/src/img/other/Mail-ikonka.svg' ?>" alt=""><?php the_field('email', 'option');?></a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-2">
                    <div class="contact-hero__address-wrap">
                        <h1 class="contact-hero__address-title"><?php the_field('adresa_spolocnosti_nadpis');?></h1>
                        <ul class="footer__address">
                            <?php
								if(have_rows('adresa', 'option') ):
									while( have_rows('adresa', 'option') ) : the_row();
							?>
								<li class="footer__address-item contact-hero__address-item"><?php the_sub_field('pridaj_adresu'); ?></li>		
							<?php
								endwhile;
								endif;
							?>
                        </ul>
                    </div>
                    <div class="contact-hero__address-wrap">
                        <h1 class="contact-hero__address-title"><?php the_field('fakturacne_udaje_nadpis');?></h1>
                        <ul class="footer__address">
                            <?php
								if(have_rows('fakturacne_udaje', 'option') ):
									while( have_rows('fakturacne_udaje', 'option') ) : the_row();
							?>
								<li class="footer__address-item contact-hero__address-item"><?php the_sub_field('pridaj_fakturacny_udaj'); ?></li>		
							<?php
								endwhile;
								endif;
							?>
                        </ul>
                    </div>
                    <div class="contact-hero__address-wrap">
                        <h1 class="contact-hero__address-title"><?php the_field('bankove_spojenie_nadpis');?></h1>
                        <ul class="footer__address ">
                            <?php
								if(have_rows('bankove_spojenie', 'option') ):
									while( have_rows('bankove_spojenie', 'option') ) : the_row();
							?>
								<li class="footer__address-item contact-hero__address-item"><?php the_sub_field('pridaj_bankove_spojenie'); ?></li>		
							<?php
								endwhile;
								endif;
							?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>  

    <?php get_template_part( 'sections/section', 'contact' );?>

    <div class="contact-map">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2614.270664206894!2d18.911033315306145!3d49.06248959426869!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4714fec1b121034b%3A0x10279424620c919e!2sBALTEUS%20spol%20s.r.o.!5e0!3m2!1ssk!2ssk!4v1622012955184!5m2!1ssk!2ssk" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
    
</section>

<?php get_footer();?>