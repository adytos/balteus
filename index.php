<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package default-theme
 */

 /*
Template Name: Blog page
*/
get_header();
?>
<section class="blog-posts">
	<div class="blog-hero">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="blog-hero__title-wrap">
						<h2 class="blog-hero__title">
                        
                             <?php  if ( pll_current_language() == 'sk'  ){
                                    the_field('nadpis_stranky_blogu', 87); 
                             } else{
                                    the_field('nadpis_stranky_blogu', 370);
                             } ?>
						</h2>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div class="blog-posts-pc-view">
        <div class="container">
            <div class="row front-blog-items wp-query-blog">   
                <?php

                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $args = [
                    
                    'post_type' => 'post',
                    "posts_per_page" => 6,
                    'paged' => $paged,
                    //'post__not_in' => array(1, 318),
                    //'order' => 'ASC'
                ];
                $query = new WP_Query($args);

                while ($query->have_posts()) : $query->the_post();
                ?>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                      <?php $post = get_post();?>
                        <div  <?php post_class("blog-front-boxs");?> >
                            <a class="blog-front-boxs__link" href="<?php the_permalink(); ?>">
                                <div class="blog-front-single">
                                    <div class="blog-front__content">
                                        <div class="blog-front__title-wrap">
                                            <?php

                                            the_title('<h1 class="blog-front__title">', '</h1>');

                                            ?>
                                        </div>
                                        <div class="blog-front__text">
                                            
                                                <?php  the_excerpt();?>
                                            
                                        </div>                        
                                    </div>
                                    <div class="blog-front__button-wrap">
                                        <a class="blog-front__button blog__button" href="<?php the_permalink(); ?>"><?php echo __('Čítať ďalej', 'default-theme');?> <img class="references-box__icon" src="<?php echo get_template_directory_uri() . '/src/img/other/arrow-right.svg' ?>" > </a>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                    <?php wp_reset_postdata() ?>
                <?php

                endwhile;

                ?>
                
                

                
                    <div class="col-12">
                        <?php
                            if (function_exists('wp_paginate')) :
                                wp_paginate($custom_query->max_num_pages);

                            endif;
                        ?>
                    </div>             
            </div>
        </div>
    </div> 
</section>
<?php

get_footer();
