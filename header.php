<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package default-theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">


	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" >
	<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">



	<?php wp_head(); ?>
</head>

<body id="home" <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" <?php body_class("site-header")?> >
		<div class="pre-header">
			<div class="container">
				<div class="row">
					<div class="col">
						<ul class="pre-header-list">
							<li class="pre-header-list__item"><a href="tel:<?php the_field('telefon', 'option');?>"><img src="<?php echo get_template_directory_uri() . '/src/img/other/Mobil_ikonka.svg'?>" alt=""><?php the_field('telefon', 'option');?></a></li>
							<li class="pre-header-list__item"><a href="mailto:<?php the_field('email', 'option');?>"><img src="<?php echo get_template_directory_uri() . '/src/img/other/Mail-ikonka.svg'?>" alt=""><?php the_field('email', 'option');?></a></li>
						</ul>
					</div>
				</div>
			</div>

		</div>
		<div class="header-bcg"  >
			<div class="container">
				<div class="row">
					<div class="col-4 col-sm-3 col-md-3 col-lg-3">
						<div class="header__logo-wrap">					
								<a href="<?php echo get_home_url(); ?>"><img class="header__logo header__logo-main" src="<?php echo get_template_directory_uri() . '/src/img/other/Logo_Balteus.svg'?>" alt=""></a>	
						</div>

					</div>
					<div class="col-4 col-sm-6 col-md-6 col-lg-7" >
						<nav id="site-navigation" class="main-navigation">
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( '', 'default-theme' ); ?>
								<span class="menu__btn-bar"></span>
                            	<span class="menu__btn-bar"></span>
                          	   <span class="menu__btn-bar"></span>
                   		       <span class="menu__btn-bar"></span>
							</button>
							<?php
							wp_nav_menu(
								array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'primary-menu',
								)
							);
							?>
						</nav>
					</div>
					<div class="col-4 col-sm-3 col-md-3 col-lg-2">
						<div class="header-search__wrap">
							<form class="search__img-wrap">
								<button  class="search__link" href=""><img class="search__img" src="<?php echo get_template_directory_uri() . '/src/img/other/search-icon.svg'?>" alt=""></button>
							</form>	
							<?php pll_the_languages();?>	
						</div>		
					</div>
				</div>
			</div>
		</div>
		<div class="search-max">
			<div class="container">
				<div class="row">
					<div class="col">
						<form action="/" class="search-max-wrap">
							<?php	if ( pll_current_language() == 'sk'  ) { ?>
								<input class="search-max-wrap__input" type="search" name="s" placeholder="Hľadaj">
							<?php	} else {  ?>
								<input class="search-max-wrap__input" type="search" name="s" placeholder="Search">
							<?php }?>
							<button type="submit" class="search-max-wrap__btn"><img class="search__img" src="<?php echo get_template_directory_uri() . '/src/img/other/search-icon-orange.svg'?>" alt=""></button>
						</form>
					</div>
				</div>
			</div>

		</div>
	</header>
