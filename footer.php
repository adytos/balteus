<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package default-theme
 */

?>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="footer__img-wrap">
						<img class="footer__img" src="<?php echo get_template_directory_uri() . '/src/img/other/balteus_logo_biele.svg' ?>" alt="">
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="footer__address-wrap">
						<ul class="footer__address">
							<?php
								if(have_rows('adresa', 'option') ):
									while( have_rows('adresa', 'option') ) : the_row();
							?>
								<li class="footer__address-item"><?php the_sub_field('pridaj_adresu'); ?></li>		
							<?php
								endwhile;
								endif;
							?>
						</ul>
						
                             <?php
                                wp_nav_menu(
                                    array(
                                        'theme_location' => 'menu-2',
                                        'menu_id'        => 'footer-menu',
                                    )
                                );
                                ?>
                    
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-3">
					<div class="footer__social-wrap">
						<ul class="footer__social">
							<li class="footer__social-item"><a class="footer__social-link" href="<?php the_field('facebook', 'option');?>" target="_blank"><img class="footer__social-img" src="<?php echo get_template_directory_uri() . '/src/img/other/Facebook_ikonka.svg' ?>" alt=""></a></li>
							<li class="footer__social-item"><a class="footer__social-link" href="<?php the_field('google', 'option');?>" target="_blank"><img class="footer__social-img" src="<?php echo get_template_directory_uri() . '/src/img/other/google_ikonka.svg' ?>" alt=""></a></li>
							<li class="footer__social-item"><a class="footer__social-link" href="<?php the_field('linkedin', 'option');?>" target="_blank"><img class="footer__social-img" src="<?php echo get_template_directory_uri() . '/src/img/other/linkedin_ikonka.svg' ?>" alt=""></a></li>
							<li class="footer__social-item"><a class="footer__social-link" href="<?php the_field('twitter', 'option');?>" target="_blank"><img class="footer__social-img" src="<?php echo get_template_directory_uri() . '/src/img/other/twitter_ikonka.svg' ?>" alt=""></a></li>
						</ul>
					</div>	
				</div>
				<div class="col-sm-12 col-md-12 col-lg-5 offset-lg-1">
					<h1 class="newsletter__title"> <?php echo __('Prihláste sa na odber newslettera','default-theme');?></h1>			
						<?php	if ( pll_current_language() == 'sk'  ) {
								echo do_shortcode('[contact-form-7 id="21" title="Newsletters"]');
							} else {  echo do_shortcode('[contact-form-7 id="432" title="Newsletters_en"]');
							 }
						?>	
				</div>
			</div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
