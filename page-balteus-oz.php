<?php 

/*
Template Name: Balteus page
*/
get_header();?>

<section class="balteus-hero">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="balteus-hero-box">
                    <h1 class="balteus-hero-box__title"><?php the_field('balteus_nadpis');?></h1>
                    <h2 class="balteus-hero-box__subtitle"><?php the_field('balteus_podnadpis');?></h2>
                    <p class="balteus-hero-box__content"><?php the_field('balteus_text');?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="work-with">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="work-with__title-wrap">
                    <h1 class="work-with__title"><?php the_field('podporujeme');?></h1>
                </div>
            </div>
            
            <?php
                if(have_rows('spolupracujeme') ):
                    while( have_rows('spolupracujeme') ) : the_row();
                ?>
             <div class="col-sm-12 col-md-6 col-lg">
                    <div class="work-with-box">
                        <img class="work-with-box__img" src="<?php echo get_sub_field('obrazok_spoluprace') ['url']; ?>">
                        <h3 class="work-with-box__title"><?php the_sub_field('popis_spoluprace'); ?></h3>
                    </div>    
            </div>
            <?php
                  endwhile;
                endif;
            ?>

        </div>
    </div>
</section>

<section class="activities">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
                <div class="activities__title-wrap">
                    <h2 class="activities__title"><?php the_field('nase_aktivity_nadpis')?></h2>
                </div>
                
                <?php
                if(have_rows('nase_aktivity') ):
                    while( have_rows('nase_aktivity') ) : the_row();
                ?>
                <div class="acordeon-wrap">
                    <dl class="acordeon">
                        <dt class="acordeon-title"><a href=""><?php the_sub_field('nadpis_aktivity'); ?></a></dt>
                        <dd class="acordeon-content"><?php the_sub_field('popis_aktivity'); ?>
                            <?php
                                    if(have_rows('vnoreny_akordeon') ):
                                        while( have_rows('vnoreny_akordeon') ) : the_row();
                                    ?>
                                    <div class="acordeon-wrap">
                                        <dl class="acordeon">
                                            <dt class="acordeon-title"><a href=""><?php the_sub_field('nadpis_vnoreneho_akordeonu'); ?></a></dt>
                                            <dd class="acordeon-content">
                                                <div><?php the_sub_field('popis_vnoreneho_akordeonu'); ?></div>
                                            </dd>
                                        <dl>
                                    </div>
                                <?php
                                    endwhile;
                                    endif;
                                ?>
                              
                        </dd>
                        
                    <dl>
                </div>
            <?php
                  endwhile;
                endif;
            ?>

            </div>
            <div class="col-12">
                <div class="btn__main-wrap">
                    <a class="btn__main" href="<?php echo get_permalink( icl_object_id(36, 'page', false) );?>"><?php echo __('Mám záujem', 'default-theme');?></a>

                </div>

            </div>
        </div>
    </div>
</section>



<?php get_footer();?>