
<?php 
/*
Template Name: Home page
*/
get_header(); ?>

    <?php
				
        get_template_part( 'sections/section', 'hero' );

        get_template_part( 'sections/section', 'about-front' );

        get_template_part( 'sections/section', 'services' );

        get_template_part( 'sections/section', 'feature' );

        get_template_part( 'sections/section', 'references-front' );

        get_template_part( 'sections/section', 'blog-front' );

        get_template_part( 'sections/section', 'companies' );
				
    ?>
    
    
<?php get_footer(); ?>
