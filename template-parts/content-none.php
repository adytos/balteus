<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package default-theme
 */

?>



<div class="col">

	<section class="no-results not-found">
		<header class="page-header">
			<h1 class="page-title"><?php //esc_html_e('Nothing Found', 'hsw-theme');
									?></h1>
		</header><!-- .page-header -->

		<div class="page-content">
			<?php
			if (is_home() && current_user_can('publish_posts')) :



			elseif (is_search()) :
			?>

				<p><?php echo __('Nič sa nenašlo', 'default-theme'); ?></p>
			<?php


			else :
			?>


			<?php


			endif;
			?>
		</div><!-- .page-content -->
	</section><!-- .no-results -->
</div>