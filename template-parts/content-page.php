<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package default-theme
 */

?>

<section class="page-footer-menu">
	<div class="page-footer-menu__bcg">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="page-footer-menu__title-wrap">
						<h1 class="page-footer-menu__title"><?php the_title();?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="page-footer-menu__content">
					<?php the_content();?>
				</div>
			</div>
		</div>
	</div>
</section>
