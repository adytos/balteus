<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package default-theme
 */

?>


<?php	if ( pll_current_language() == 'sk'  ) { ?>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <article class="search-box blog-front-boxs" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <?php if ('post' === get_post_type()) : ?>
                <?php endif; ?>
            </header><!-- .entry-header -->
            <?php the_title(sprintf('<h2 class="entry-title entry-title-search blog-front__content"><a class="blog-front__title" href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
            
            
        </article><!-- #post-<?php the_ID(); ?> -->
    </div>
<?php } else{?>

    <div class="col-sm-12 col-md-6 col-lg-4">
        <article class="search-box blog-front-boxs" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <?php if ('post' === get_post_type()) : ?>
                <?php endif; ?>
            </header><!-- .entry-header -->
            <?php the_title(sprintf('<h2 class="entry-title entry-title-search blog-front__content"><a class="blog-front__title" href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
            
            
        </article><!-- #post-<?php the_ID(); ?> -->
    </div>

<?php }?>